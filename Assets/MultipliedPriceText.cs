﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MultipliedPriceText : MonoBehaviour
{
    public InAppPurchase IAP;
    public float Multiplier;
    public bool RoundPrice;
    public bool AllCaps;
    private Text _text;

    void Awake()
    {
        _text = GetComponent<Text>();
    }

    void Start()
    {
        IAP = PriceTierSplitTester.GetLifetimeIAP();
        IapManager.Instance.InitializationComplete += (b) => UpdateLabel();
        UpdateLabel();
    }

    void UpdateLabel()
    {
        if (_text == null) return;

        Multiplier = RMConfig.GetFloat("SalePriceMultiplier_float");
        float price = IapManager.Instance.GetPrice(IAP.ProductId) * Multiplier;
        if(RoundPrice) price = Mathf.Round(price) - 0.01f;

        _text.text = price.ToString("0.00") + IapManager.Instance.GetCurrencyCode(IAP.ProductId);

        _text.text = AllCaps ? _text.text.ToUpper() : _text.text;
    }
}
