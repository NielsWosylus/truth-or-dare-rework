﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

public class CodeGenerator : MonoBehaviour
{
    private const int LetterTotalValue = 10;

    private static readonly Dictionary<int, string> LetterNumbers = new Dictionary<int, string>
    {
        {1, "A"},
        {2, "B"},
        {3, "C"},
        {4, "D"},
        {5, "E"},
        {6, "F"},
        {7, "G"},
    };

    public static string GenerateCode()
    {
        return GenerateLetters() + GenerateNumbers();
    }

    private static string GenerateLetters()
    {
        List<int> results = new List<int>(3);

        results.Add(UnityEngine.Random.Range(1, 7 - 1));
        results.Add(UnityEngine.Random.Range(Mathf.Max(LetterTotalValue - results[0] - 7, 1), Mathf.Min(LetterTotalValue - results[0], 7)));
        results.Add(10 - results[0] - results[1]);
        results.Shuffle();

        var x = LetterNumbers[results[0]];
        var y = LetterNumbers[results[1]];
        var z = LetterNumbers[results[2]];

        return x + y + z;
    }

    private static string GenerateNumbers()
    {
        int day = DateTime.Now.Day;
        int hour = DateTime.Now.Hour;

        string d;
        string h;

        if (day < 10)
            d = "0" + day;
        else d = day.ToString();

        if (hour < 10)
            h = "0" + hour;
        else h = hour.ToString();

        return d + h;
    }


}
