﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AB_Enabler : MonoBehaviour
{
    public string RemoteConfigKey;
    public bool EnableWhenTrue;
    public bool OnlyApplyOnStart;

    void Start()
    {
        CheckAndEnable();
        if(!OnlyApplyOnStart)
            RMConfig.ConfigLoaded += CheckAndEnable;
    }

    void CheckAndEnable()
    {
        bool fetchedValue = RMConfig.GetBool(RemoteConfigKey);
        if(EnableWhenTrue) gameObject.SetActive(fetchedValue);
            else gameObject.SetActive(!fetchedValue);
    }
}
