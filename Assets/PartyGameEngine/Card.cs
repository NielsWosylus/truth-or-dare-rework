﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card
{
    public Player Player;
    public string Message;
    public CardType Type;
}

public enum CardType
{
    Statement,
    Truth,
    Dare,
    Joker
}
