﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeArea : MonoBehaviour
{
    public bool OverrideSafeArea;
    public float OverrideX;
    public float OverrideY;
    public float OverrideWidth;
    public float OverrideHeight;

    void Awake()
    {
        RectTransform myTransform = GetComponent<RectTransform>();
        var rect = Screen.safeArea;

        if(OverrideSafeArea)
            rect = new Rect(OverrideX, OverrideY, OverrideWidth, OverrideHeight);

        if (rect.x == 0 && rect.y == 0 && rect.width == 0 && rect.height == 0)
            return;

        myTransform.offsetMin = new Vector2(rect.x, rect.y);
        myTransform.offsetMax = new Vector2(rect.width + rect.x - Screen.width, rect.height - Screen.height);
    }
}
