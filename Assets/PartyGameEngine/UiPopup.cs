﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiPopup : MonoBehaviour
{
    public GameObject Root;

    public void Open()
    {
        gameObject.SetActive(true);
        Root.SetActive(true);
    }

    public void Close()
    {
        gameObject.SetActive(false);
        Root.SetActive(false);
    }
}
