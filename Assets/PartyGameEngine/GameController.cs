﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController Instance;

    private Card _currentCard;
    public event Action<Card> CardChanged;
    public Card CurrentCard
    {
        get => _currentCard;
        set
        {
            if (_currentCard != value)
            {
                _currentCard = value;
                CardChanged?.Invoke(value);
            }
        }
    }

    private Deck _currentDeck;
    public event Action<Deck> DeckChanged;
    public Deck CurrentDeck
    {
        get => _currentDeck;
        set
        {
            if (_currentDeck != value)
            {
                _currentDeck = value;
                DeckChanged?.Invoke(value);
            }
        }
    }

    public LanguageManager LanguageManager;
    public PlayerManager PlayerManager;
    public List<Deck> AllDecks;
    public string PrivacyPolicyURL;

    void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(this);

        Application.targetFrameRate = 60;
        UnityEngine.Random.InitState(DateTime.Now.Second);
        LanguageManager.Init();
        PlayerManager.Init();
        LanguageManager.LanguageChanged += LoadDecks;
        LoadDecks(LanguageManager.CurrentLanguage);
    }

    public void SelectDeck(Deck deck)
    {
        CurrentDeck = deck;
    }

    public void SetNextDare()
    {
        //CurrentCard = CurrentDeck.GetNextDare(PlayerManager.GetNextPlayer());
    }

    public void SetNextTruth()
    {
        //CurrentCard = CurrentDeck.GetNextTruth(PlayerManager.GetNextPlayer());
    }

    void LoadDecks(Language fromLanguage)
    {
        foreach(var deck in AllDecks)
        {
            deck.LoadContent(fromLanguage);
        }
    }

    public void OpenPrivacyPolicy()
    {
        Application.OpenURL(PrivacyPolicyURL);
    }
}
