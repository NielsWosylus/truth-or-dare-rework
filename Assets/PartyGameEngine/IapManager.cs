﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

// Deriving the IapManager class from IStoreListener enables it to receive messages from Unity Purchasing.
public class IapManager : MonoBehaviour, IStoreListener
{
    public static IapManager Instance;
    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

    public event Action<bool> InitializationComplete;

    public InAppPurchase[] InAppPurchases;
    private readonly Dictionary<string, InAppPurchase> _iapMap = new Dictionary<string, InAppPurchase>();

    public string GetPriceString(string productId)
    {
        if (!IsInitialized())
            return "0.00$";

        if (m_StoreController.products == null)
            Debug.LogError("No products found");

        if (m_StoreController.products.WithID(productId) == null)
            Debug.LogError("No IAP found with id " + productId);

        if (m_StoreController.products.WithID(productId).metadata == null)
            Debug.LogError("No meta data found for IAP with id " + productId);

        return m_StoreController.products.WithID(productId).metadata.localizedPriceString;
    }

    public float GetPrice(string productId)
    {
        if (!IsInitialized())
            return 0;

        if (m_StoreController.products == null)
            Debug.LogError("No products found");

        if (m_StoreController.products.WithID(productId) == null)
            Debug.LogError("No IAP found with id " + productId);

        if (m_StoreController.products.WithID(productId).metadata == null)
            Debug.LogError("No meta data found for IAP with id " + productId);

        return (float)m_StoreController.products.WithID(productId).metadata.localizedPrice;
    }

    public string GetCurrencyCode(string productId)
    {
        if (!IsInitialized())
            return "$";

        if (m_StoreController.products.WithID(productId) == null)
        {
            Debug.LogError("[IAPManager] product could not be found with id " + productId);
            return "";
        }

        string str = m_StoreController.products.WithID(productId).metadata.localizedPriceString;
        char[] cs = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.', ',' };
        foreach (var c in cs) { str = str.Replace(c.ToString(), ""); }

        return str;
    }

    public InAppPurchase GetIAP(string productName)
    {
        return _iapMap[productName];
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            InitializePurchasing();
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);
        }
    }

    public InAppPurchase GetIap(string productId)
    {
        return _iapMap[productId];
    }

    public bool IapExists(string productId)
    {
        return _iapMap.ContainsKey(productId);
    }
    
    public void InitializePurchasing() 
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }
        
        // Create a builder, first passing in a suite of Unity provided stores.
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        bool iOS = false;
#if UNITY_IOS
        iOS = true;
#endif

        foreach (var iap in InAppPurchases)
        {
            iap.Load();
            _iapMap.Add(iap.ProductId, iap);

            if(iOS && iap.AvailableOniOS || !iOS)
            builder.AddProduct(iap.ProductId, iap.Type, new IDs
            {
                { iap.ProductId, AppleAppStore.Name },
                { iap.ProductId, GooglePlay.Name },
            });
        }

        // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
        // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
        UnityPurchasing.Initialize(this, builder);

        //Load legacy data
        LegacyDataManager.Load(GetIAP("truthordare3.dirty"));
    }
    
    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }
    
    public void BuyProductID(string productId)
    {
        // If Purchasing has been initialized ...
        if (IsInitialized())
        {
            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
            Product product = m_StoreController.products.WithID(productId);
            
            // If the look up found a product for this device's store and that product is ready to be sold ... 
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
                m_StoreController.InitiatePurchase(product);
            }
            // Otherwise ...
            else
            {
                // ... report the product look-up failure situation  
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        // Otherwise ...
        else
        {
            // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
            // retrying initiailization.
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }
    
    
    // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
    // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
    public void RestorePurchases()
    {
        // If Purchasing has not yet been set up ...
        if (!IsInitialized())
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }
        
        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer || 
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            // ... begin restoring purchases
            Debug.Log("RestorePurchases started ...");
            
            // Fetch the Apple store-specific subsystem.
            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result) => {
                // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                // no purchases are available to be restored.
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        // Otherwise ...
        else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }
    
    
    //  
    // --- IStoreListener
    //
    
    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Purchasing has succeeded initializing. Collect our Purchasing references.
        Debug.Log("OnInitialized: PASS");
        
        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;

        var m_AppleExtensions = extensions.GetExtension<IAppleExtensions>();
        Dictionary<string, string> introductory_info_dict = m_AppleExtensions.GetIntroductoryPriceDictionary();

        Debug.Log("Available items:");
        foreach (var item in controller.products.all)
        {
            if (item.availableToPurchase)
            {
                Debug.Log(string.Join(" - ",
                    new[]
                    {
                        item.metadata.localizedTitle,
                        item.metadata.localizedDescription,
                        item.metadata.isoCurrencyCode,
                        item.metadata.localizedPrice.ToString(),
                        item.metadata.localizedPriceString,
                        item.transactionID,
                        item.receipt
                    }));

                // this is the usage of SubscriptionManager class
                if (item.receipt != null)
                {
                    if (item.definition.type == ProductType.Subscription)
                    {
                        if (checkIfProductIsAvailableForSubscriptionManager(item.receipt))
                        {
                            string intro_json = (introductory_info_dict == null || !introductory_info_dict.ContainsKey(item.definition.storeSpecificId)) ? null : introductory_info_dict[item.definition.storeSpecificId];
                            SubscriptionManager p = new SubscriptionManager(item, intro_json);
                            SubscriptionInfo info = p.getSubscriptionInfo();

                            var iap = GetIap(info.getProductId());
                            if (info.isSubscribed() == Result.True)
                            {
                                iap.OnPurchaseSuccess();
                            }
                            else
                            {
                                if(iap.IsOwned) iap.Expire();
                            }

                            Debug.Log("product id is: " + info.getProductId());
                            Debug.Log("purchase date is: " + info.getPurchaseDate());
                            Debug.Log("subscription next billing date is: " + info.getExpireDate());
                            Debug.Log("is subscribed? " + info.isSubscribed().ToString());
                            Debug.Log("is expired? " + info.isExpired().ToString());
                            Debug.Log("is cancelled? " + info.isCancelled());
                            Debug.Log("product is in free trial peroid? " + info.isFreeTrial());
                            Debug.Log("product is auto renewing? " + info.isAutoRenewing());
                            Debug.Log("subscription remaining valid time until next billing date is: " + info.getRemainingTime());
                            Debug.Log("is this product in introductory price period? " + info.isIntroductoryPricePeriod());
                            Debug.Log("the product introductory localized price is: " + info.getIntroductoryPrice());
                            Debug.Log("the product introductory price period is: " + info.getIntroductoryPricePeriod());
                            Debug.Log("the number of product introductory price period cycles is: " + info.getIntroductoryPricePeriodCycles());

                        }
                        else
                        {
                            Debug.Log("This product is not available for SubscriptionManager class, only products that are purchase by 1.19+ SDK can use this class.");
                        }
                    }
                    else
                    {
                        Debug.Log("the product is not a subscription product");
                    }
                }
                else
                {
                    Debug.Log("the product should have a valid receipt");
                }
            }
        }

        InitializationComplete?.Invoke(true);
    }

    private bool checkIfProductIsAvailableForSubscriptionManager(string receipt)
    {
        var receipt_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(receipt);
        if (!receipt_wrapper.ContainsKey("Store") || !receipt_wrapper.ContainsKey("Payload"))
        {
            Debug.Log("The product receipt does not contain enough information");
            return false;
        }
        var store = (string)receipt_wrapper["Store"];
        var payload = (string)receipt_wrapper["Payload"];

        if (payload != null)
        {
            switch (store)
            {
                case GooglePlay.Name:
                    {
                        var payload_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(payload);
                        if (!payload_wrapper.ContainsKey("json"))
                        {
                            Debug.Log("The product receipt does not contain enough information, the 'json' field is missing");
                            return false;
                        }
                        var original_json_payload_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode((string)payload_wrapper["json"]);
                        if (original_json_payload_wrapper == null || !original_json_payload_wrapper.ContainsKey("developerPayload"))
                        {
                            Debug.Log("The product receipt does not contain enough information, the 'developerPayload' field is missing");
                            return false;
                        }
                        var developerPayloadJSON = (string)original_json_payload_wrapper["developerPayload"];
                        var developerPayload_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(developerPayloadJSON);
                        if (developerPayload_wrapper == null || !developerPayload_wrapper.ContainsKey("is_free_trial") || !developerPayload_wrapper.ContainsKey("has_introductory_price_trial"))
                        {
                            Debug.Log("The product receipt does not contain enough information, the product is not purchased using 1.19 or later");
                            return false;
                        }
                        return true;
                    }
                case AppleAppStore.Name:
                case AmazonApps.Name:
                case MacAppStore.Name:
                    {
                        return true;
                    }
                default:
                    {
                        return false;
                    }
            }
        }
        return false;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);

        InitializationComplete?.Invoke(false);
    }
    
    
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        string key = args.purchasedProduct.definition.id;

        if (!IapExists(key))
        {
            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
            return PurchaseProcessingResult.Complete;
        }

        Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        var iap = GetIap(key);
        iap.OnPurchaseSuccess();

        return PurchaseProcessingResult.Complete;
    }
    
    
    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));

        string key = product.definition.id;
        var iap = GetIap(key);
        iap.OnPurchaseFailed();



        //bool showPopup = true;
        //string message = "An unknown error occured. The developer has been notified.";

        //switch(failureReason)
        //{
        //    case PurchaseFailureReason.DuplicateTransaction:
        //        message = "Transaction was already completed.";
        //        break;

        //    case PurchaseFailureReason.ExistingPurchasePending:
        //        message = "Another purchase is pending. Please wait and try again.";
        //        break;

        //    case PurchaseFailureReason.PaymentDeclined:
        //        message = "An unknown error occured. The developer has been notified.";
        //        break;

        //    case PurchaseFailureReason.ProductUnavailable:
        //        message = "Product was unavailable. The developer has been notified.";
        //        break;

        //    case PurchaseFailureReason.PurchasingUnavailable:
        //        message = "Purchasing unavailable. Please check your internet connection.";
        //        break;

        //    case PurchaseFailureReason.SignatureInvalid:
        //        message = "An unknown error occured. The developer has been notified.";
        //        break;

        //    case PurchaseFailureReason.UserCancelled:
        //        showPopup = false;
        //        break;

        //    case PurchaseFailureReason.Unknown:
        //        message = "An unknown error occured. The developer has been notified.";
        //        break;
        //}
    }

    void OnApplicationFocus(bool isFocused)
    {
        Instance = this;
    }

    void OnApplicationPause(bool isPaused)
    {
        Instance = this;
    }
}