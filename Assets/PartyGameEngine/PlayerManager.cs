﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager Instance;

    private static Player _currentPlayer;
    public static Action<Player> CurrentPlayerChanged;
    public static Player CurrentPlayer
    {
        get => _currentPlayer;
        set
        {
            if(_currentPlayer != value)
                CurrentPlayerChanged?.Invoke(value);
            _currentPlayer = value;
        }
    }

    public bool RandomizePlayerOrder;
    public int MaxPlayerCount;
    public int PlayerCount;
    public Color[] DefaultPlayerColors;
    public List<Player> Players;

    private const string FileName = "Players.bin";
    private int _currentPlayerIndex;

    public static Player GetPlayer(int index)
    {
        return Instance.Players[index];
    }

    public void SetNextPlayer()
    {
        if (RandomizePlayerOrder)
        {
            int index = UnityEngine.Random.Range(0, PlayerCount - 1);
            CurrentPlayer = Players[index];
        }

        CurrentPlayer = Players[_currentPlayerIndex];

        _currentPlayerIndex++;
        if (_currentPlayerIndex >= PlayerCount)
            _currentPlayerIndex = 0;
    }

    public void Init()
    {
        Instance = this;
        DontDestroyOnLoad(this);

        if (LocalData.Exists(FileName))
        {
            Players = LocalData.Load<List<Player>>(FileName);
            for (int i = 0; i < MaxPlayerCount; i++)
            {
                Players[i].Color = DefaultPlayerColors[i];
            }
            return;
        }

        Players = new List<Player>();
        for (int i = 0; i < MaxPlayerCount; i++)
        {
            Players.Add(new Player {Color = DefaultPlayerColors[i], Name = LanguageManager.GetLabel("player") + " " + (i + 1)});
        }
    }

    public void SavePlayers()
    {
        LocalData.Save(FileName, Players);
    }
}
