﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupManager : MonoBehaviour
{
    private static PopupManager _instance;
    public float SecondsUntilNextPopup;
    public int CardsUntilNextPopup;

    private bool _pauseThisFrame;

    void Awake()
    {
        _instance = this;
    }

    void Update()
    {
        if(!_pauseThisFrame)
            SecondsUntilNextPopup -= Time.deltaTime;
        _pauseThisFrame = false;
    }

    public static bool ItIsTimeForAPopup()
    {
        return _instance.SecondsUntilNextPopup <= 0 && _instance.CardsUntilNextPopup <= 0;
    }

    public static void PauseForOneFrame()
    {
        _instance._pauseThisFrame = true;
    }
}
