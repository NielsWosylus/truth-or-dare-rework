﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//A collection of all content loaded from a language
public class ContentCollection : Dictionary<string, List<string>>
{
    public StringContainer GetStringContainer(string fromDeckName)
    {
        return new StringContainer(this[fromDeckName]);
    }
}
