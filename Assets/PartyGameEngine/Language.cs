﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

[CreateAssetMenu]
public class Language : ScriptableObject
{
    public string LanguageCode;
    public TextAsset ContentFile;
    public TextAsset LabelsFile;
    public Sprite Flag;
    public SystemLanguage SystemLanguage;
    [NonSerialized] public ContentCollection Content;
    [NonSerialized] public bool IsLoaded;
    [NonSerialized] public Dictionary<string, string> Labels;

    public void Load()
    {
        Labels = JsonConvert.DeserializeObject<Dictionary<string, string>>(LabelsFile.text);
        Content = JsonConvert.DeserializeObject<ContentCollection>(ContentFile.text);
        IsLoaded = true;
    }
}
