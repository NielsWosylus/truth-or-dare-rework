﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class LocalData
{
    public static string DirectoryPath
    {
        get { return Application.persistentDataPath + "/localdata"; }
    }

    public static void Save<T>(string fileName, T data)
    {
        string path = DirectoryPath + "/" + fileName;
        string dir = Path.GetDirectoryName(path);
        if (!Directory.Exists(dir))
            Directory.CreateDirectory(dir);

        using (FileStream fileStream = File.Open(path, FileMode.Create))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(fileStream, data);
        }
    }

    public static T Load<T>(string fileName)
    {
        string path = DirectoryPath + "/" + fileName;
        T loadedData = default(T);

        try
        {
            if (!File.Exists(path))
                return loadedData;

            using (FileStream fileStream = File.Open(path, FileMode.Open))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                loadedData = (T)formatter.Deserialize(fileStream);
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }

        return loadedData;
    }

    public static bool Exists(string fileName)
    {
        try
        {
            return File.Exists(DirectoryPath + "/" + fileName);
        }
        catch (Exception e)
        {
            Debug.Log(e);
            return false;
        }
    }
}