﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PriceTierSplitTester : MonoBehaviour
{
    private static PriceTierSplitTester Instance;
    public string AppId;
    public Deck Deck;
    public InAppPurchase DefaultIAP;
    public PurchaseOption Option;


    void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(this);
    }

    public static InAppPurchase GetLifetimeIAP()
    {
#if UNITY_IOS
        return Instance.DefaultIAP;
#endif

        if (!RMConfig.GetBool("UseNewPriceTiers_bool"))
            return Instance.DefaultIAP;

        int priceTier = RMConfig.GetInt("LifetimePriceTier_int");
        return GetIAP(Instance.Deck, Instance.Option, priceTier);
    }

    private static InAppPurchase GetIAP(Deck deck, PurchaseOption option, int priceTier)
    {
        string id = Instance.AppId + "." + deck.Identifier + ".lifetime.tier" + priceTier;
        return IapManager.Instance.GetIAP(id);
    }
}

public enum PurchaseOption
{
    Lifetime,
    Rent72h,
    Rent12h
}