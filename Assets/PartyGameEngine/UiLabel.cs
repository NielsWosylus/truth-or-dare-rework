﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UiLabel : MonoBehaviour
{
    public string Key;
    public bool AllCaps;
    public event Action UpdatedText;
    public Text Text {get; private set;}

    public void SetKey(string key)
    {
        Key = key;
        UpdateLabel();
    }

    void Awake()
    {
        Text = GetComponent<Text>();
    }

    void Start()
    {
        LanguageManager.LanguageChanged += l => UpdateLabel();
        UpdateLabel();
    }

    void UpdateLabel()
    {
        if (Text == null) return;

        if (string.IsNullOrEmpty(Key))
        {
            Text.text = string.Empty;
            UpdatedText?.Invoke();
            return;
        }

        string lbl = LanguageManager.GetLabel(Key);
        Text.text = AllCaps ? lbl.ToUpper() : lbl;
        UpdatedText?.Invoke();
    }
}
