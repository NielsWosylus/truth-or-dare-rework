﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Firebase.Analytics;
using UnityEngine;

public class RatingManager : MonoBehaviour
{
#if (UNITY_IOS && !UNITY_EDITOR)
    [DllImport("__Internal")]
    extern static private bool _reviewControllerIsAvailable();
    [DllImport("__Internal")]
    extern static private bool _reviewControllerShow();   
#endif

    public string AppleId;
    public string GoogleId;

    public static bool HasRated => LocalData.Exists("HasRated.bin");
    public static bool DoNotAsk => LocalData.Exists("DoNotAsk.bin") || HasRated;
    public static event Action UserRatedApp;
    private static string _appleId;
    private static string _googleId;

    void Awake()
    {
        _googleId = GoogleId;
        _appleId = AppleId;
    }

    public void RateFromUi()
    {
        FirebaseAnalytics.LogEvent("Ratings_Rate");
        RateApp();
    }

    public static void NeverAskAgain()
    {
        LocalData.Save("DoNotAsk.bin", true);
    }

    public static void RateApp()
    {
        //iOS
#if (UNITY_IOS && !UNITY_EDITOR)
        if (_reviewControllerIsAvailable())
        {
            _reviewControllerShow();
            return;
        }
        else
        {
            Application.OpenURL("https://itunes.apple.com/app/id" + _appleId +"?action=write-review");
        }
#endif

        //Android
#if UNITY_ANDROID
        Application.OpenURL("https://play.google.com/store/apps/details?id=" + _googleId);
#endif

        LocalData.Save("HasRated.bin", true);
        UserRatedApp?.Invoke();
    }
}

