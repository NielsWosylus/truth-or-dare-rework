﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageManager : MonoBehaviour
{
    private static Language _currentLanguage;
    public static event Action<Language> LanguageChanged;
    public static Language CurrentLanguage
    {
        get => _currentLanguage;

        set
        {
            _currentLanguage = value;
            if (!_currentLanguage.IsLoaded) { _currentLanguage.Load(); }
            LanguageChanged?.Invoke(value);
        }
    }

    public static string GetLabel(string key)
    {
        if (_currentLanguage == null)
        {
            Debug.LogError("Cannot get label " + key + ": no language set");
            return key;
        }

        if (!_currentLanguage.Labels.ContainsKey(key))
        {
            Debug.LogWarning("Cannot get label " + key + ": key not found");
            return key;
        }

        return _currentLanguage.Labels[key];
    }

    public Language DefaultLanguage;
    public List<Language> AvailableLanguages;
    private const string FileName = "Language.bin";

    public void Init()
    {
        if (LocalData.Exists(FileName))
        {
            string loadedLanguageCode = LocalData.Load<string>(FileName);
            foreach (var language in AvailableLanguages)
            {
                if (language.LanguageCode == loadedLanguageCode)
                {
                    CurrentLanguage = language;
                    return;
                }
            }
        }

        foreach (var language in AvailableLanguages)
        {
            if (Application.systemLanguage == language.SystemLanguage)
            {
                CurrentLanguage = language;
                return;
            }
        }

        CurrentLanguage = DefaultLanguage;
    }
}
