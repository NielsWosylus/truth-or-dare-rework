﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyUtil {

    public static Color ColorWithNewAlpha(Color originalColor, float alpha)
    {
        return new Color(originalColor.r, originalColor.g, originalColor.b, alpha);
    }
}
