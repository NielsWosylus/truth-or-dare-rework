﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Deck : ScriptableObject
{
    public string Identifier;
    public InAppPurchase[] InAppPurchases;

    public bool IsUnlocked
    {
        get
        {
            if (InAppPurchases == null)
                return true;

            if (InAppPurchases.Length == 0)
                return true;

            foreach (var inAppPurchase in InAppPurchases)
            {
                if (inAppPurchase.IsOwned)
                    return true;
            }

            return false;
        }
    }

    private StringContainer _truths;
    private StringContainer _dares;
    private StringContainer _jokers;

    public Card GetNextTruth()
    {
        return new Card {Message = _truths.GetNextString(), Type = CardType.Truth};
    }

    public Card GetNextTruth(Player forPlayer)
    {
        return new Card {Message = _truths.GetNextString(), Player = forPlayer, Type = CardType.Truth};
    }

    public Card GetNextDare()
    {
        return new Card { Message = _dares.GetNextString(), Type = CardType.Truth };
    }

    public Card GetNextDare(Player forPlayer)
    {
        return new Card { Message = _dares.GetNextString(), Player = forPlayer, Type = CardType.Truth };
    }

        public Card GetNextJoker()
    {
        return new Card { Message = _jokers.GetNextString(), Type = CardType.Joker };
    }

    public void LoadContent(Language fromLanguage)
    {
        _truths = fromLanguage.Content.GetStringContainer(Identifier + "_truth");
        _dares = fromLanguage.Content.GetStringContainer(Identifier + "_dare");
        _jokers = fromLanguage.Content.GetStringContainer(Identifier + "_joker");
    }
}
