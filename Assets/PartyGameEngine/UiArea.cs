﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class UiArea : MonoBehaviour
{
    public static UiArea OpenArea;
    public static event Action<UiArea> OpenAreaChanged;

    public Action Opened;
    public Action Closed;
    private UiArea _previousArea;

    void Awake()
    {
        if (OpenArea == null)
            OpenArea = this;
    }

    public void Open()
    {
        if (OpenArea == this) return;

        _previousArea = OpenArea;
        OpenArea = this;
        OpenAreaChanged?.Invoke(this);
        Opened?.Invoke();
        OnOpen();

        _previousArea?.Close();
    }

    public void Close()
    {
        OnClose();
        Closed?.Invoke();
    }

    public void OpenPrevious()
    {
        if (_previousArea == null)
        {
            Debug.LogError("No previous area to open");
            return;
        }

        _previousArea.Open();
    }

    protected virtual void OnClose()
    {
        gameObject.SetActive(false);
    }

    protected virtual void OnOpen()
    {
        gameObject.SetActive(true);
    }

    void OnEnable()
    {
#if UNITY_EDITOR
        Open();
#endif
    }
}