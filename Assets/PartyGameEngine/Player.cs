﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Player
{
    public string Name;
    [NonSerialized] public Color Color;
    public Gender Gender;
}

public enum Gender
{
    Undefined,
    Male,
    Female,
    Other
}
