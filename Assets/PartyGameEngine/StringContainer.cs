﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

public class StringContainer
{
    public StringContainer(List<string> strings)
    {
        _strings = strings;
        _strings.Shuffle();
    }

    private List<string> _strings;
    private int _index;

    public void Shuffle()
    {
        _strings.Shuffle();
    }

    public string GetNextString()
    {
        string re = _strings[_index];

        _index++;
        if (_index >= _strings.Count)
        {
            _strings.Shuffle();
            _index = 0;
        }

        return re;
    }
}
