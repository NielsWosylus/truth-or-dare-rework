﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

[CreateAssetMenu]
public class InAppPurchase : ScriptableObject
{
    public string ProductId;
    public bool AvailableOniOS = true;
    public ProductType Type;
    [NonSerialized] public bool IsOwned;

    public event Action Activated;
    public event Action Expired;
    public event Action<bool> PurchaseComplete;
    public event Action PurchaseBegun;

    private string FileName => ProductId + ".bin";

    public void Purchase()
    {
        PurchaseBegun?.Invoke();
        IapManager.Instance.BuyProductID(ProductId);
    }

    public void OnPurchaseSuccess()
    {
        IsOwned = true;
        Save();
        Activate();
        Debug.Log(ProductId + ": purchase succesful");
        PurchaseComplete?.Invoke(true);
    }

    public void OnPurchaseFailed()
    {
        Debug.Log(ProductId + ": purchase failed");
        PurchaseComplete?.Invoke(false);
    }

    public void Expire()
    {
        IsOwned = false;
        Save();
        Expired?.Invoke();
    }

    public void Load()
    {
        if(Type != ProductType.Consumable)
            IsOwned = LocalData.Load<bool>(FileName);

        if(IsOwned) Activate();
    }

    private void Save()
    {
        if(Type != ProductType.Consumable)
            LocalData.Save(FileName, true);
    }

    private void Activate()
    {
        Activated?.Invoke();
    }
}
