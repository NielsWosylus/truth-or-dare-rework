﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeckSelectorButton : MonoBehaviour
{
    public Deck Deck;
    public Image Image;
    public UiPopup PurchasePanel;
    public Color SelectedColor;
    public Color DeselectedColor;

    void Awake()
    {
        GameController.Instance.DeckChanged += DeckChanged;
        DeckChanged(GameController.Instance.CurrentDeck);
    }

    public void Clicked()
    {
        if(!Deck.IsUnlocked)
        {
            PurchasePanel.Open();
            return;
        }

        GameController.Instance.SelectDeck(Deck);
    }

    void DeckChanged(Deck deck)
    {
        Image.color = deck == Deck ? SelectedColor : DeselectedColor;
    }
}
