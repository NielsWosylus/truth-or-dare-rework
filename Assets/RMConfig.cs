﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.RemoteConfig;
using System;

public class RMConfig : MonoBehaviour
{
    public static event Action ConfigLoaded;
    private static bool _overrideFetchedValues;
    private static readonly Dictionary<string, object> Defaults = new Dictionary<string, object>();


    public static bool GetBool(string key)
    {
        bool value;
        if(_overrideFetchedValues)
            value = (bool)Defaults[key];
        else value = FirebaseRemoteConfig.GetValue(key).BooleanValue;

        Debug.Log($"Getting RM value {key}: {value}");
        return value;
    }

    public static float GetFloat(string key)
    {
        float value;
        if (_overrideFetchedValues)
            value = (float)Defaults[key];
        else value = (float)FirebaseRemoteConfig.GetValue(key).DoubleValue;

        Debug.Log($"Getting RM value {key}: {value}");
        return value;
    }

    public static int GetInt(string key)
    {
        int value;
        if (_overrideFetchedValues)
            value = (int)Defaults[key];
        else value = (int)FirebaseRemoteConfig.GetValue(key).DoubleValue;

        Debug.Log($"Getting RM value {key}: {value}");
        return value;
    }

    public static string GetString(string key)
    {
        return FirebaseRemoteConfig.GetValue(key).StringValue;
    }

    public bool OverrideFetchedValues;
    public RMSetting[] DefaultSettings;


    void Awake()
    {
        UnityEngine.Random.InitState(DateTime.Now.Second);
        _overrideFetchedValues = OverrideFetchedValues;
        foreach(var setting in DefaultSettings)
        {
            Defaults.Add(setting.Key, setting.Value);
        }

        FirebaseRemoteConfig.SetDefaults(Defaults);

        if(!OverrideFetchedValues)
            StartCoroutine(LoadingConfig());
    }

    IEnumerator LoadingConfig()
    {
        bool configLoaded = false;

        FirebaseRemoteConfig.FetchAsync().ContinueWith(task =>
        {
            if (task.IsCompleted && !task.IsFaulted)
            {
                Debug.Log("Fetched config values.");
                FirebaseRemoteConfig.ActivateFetched();
            }
            else
            {
                Debug.LogWarning("Failed to fetch config values.");
            }

            configLoaded = true;
        });

        while (!configLoaded)
            yield return null;

        ConfigLoaded?.Invoke();
    }
}

[Serializable]
public class RMSetting
{
    public string Key;
    public RMType Type;
    public RMDefaultBoolValue BoolValue;
    public float FloatValue;

    public object Value
    {
        get
        {
            switch (Type)
            {
                case RMType.Bool:
                    if (BoolValue == RMDefaultBoolValue.True) return true;
                    else if (BoolValue == RMDefaultBoolValue.False) return false;
                    else return UnityEngine.Random.Range(0, 1f) >= 0.5f;

                case RMType.Float: return FloatValue;

                default: return false;
            }
        }
    }
}

public enum RMDefaultBoolValue
{
    True,
    False,
    FiftyFifty,
}

public enum RMType
{
    Bool,
    Float
}
