﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GeneratedCodeText : MonoBehaviour
{
    void Start()
    {
        GetComponent<Text>().text = CodeGenerator.GenerateCode();
    }
}
