﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class QuickAnimation : MonoBehaviour
{
    public float AnimationTime;
    public float AnimationDelay;
    public AnimationCurve AnimationCurve;

    public RectTransform RectTransform;

    public RectTransform GetStartFrom;
    public RectTransform GetEndFrom;
    public QuickAnimationPoint Start;
    public QuickAnimationPoint End;

    public UnityEvent AnimationComplete;
    public event System.Action Started;

    private float _delay;
    private float _time;
    public bool IsPlaying {get; private set;}
    private bool _isReversing;

    [ContextMenu("Set Start")]
    public void SetStart()
    {
        Start = new QuickAnimationPoint(RectTransform);
    }

    [ContextMenu("Set End")]
    public void SetEnd()
    {
        End = new QuickAnimationPoint(RectTransform);
    }

    [ContextMenu("Display Start")]
    public void DisplayStart()
    {
        Apply(Start);
    }

    [ContextMenu("Display End")]
    public void DisplayEnd()
    {
        Apply(End);
    }

    public void Apply(QuickAnimationPoint point)
    {
        RectTransform.anchoredPosition = new Vector2(point.PositionX, point.PositionY);
        RectTransform.rotation = Quaternion.Euler(0, 0, point.Rotation);
    }

    [ContextMenu("Play Animation")]
    public void PlayAnimation()
    {
        Started?.Invoke();
        
        if(GetStartFrom != null)
            Start = new QuickAnimationPoint(GetStartFrom);

        if(GetEndFrom != null)
            End = new QuickAnimationPoint(GetEndFrom);

        Apply(Start);
        _delay = AnimationDelay;
        _time = 0;
        _isReversing = false;
        IsPlaying = true;
    }

    public void PlayAnimationInReverse()
    {
        _isReversing = true;
        IsPlaying = true;
    }

    public void ResetAnimation()
    {
        Apply(Start);
        _delay = AnimationDelay;
        _time = 0;
        _isReversing = false;
        IsPlaying = false;
    }

    public void Pause()
    {
        IsPlaying = false;
    }

    void Update()
    {
        if (!IsPlaying)
            return;

        if (_delay > 0)
        {
            _delay -= Time.deltaTime;
            return;
        }

        if (!_isReversing)
        {
            if (_time < 1f)
            {
                _time += Time.deltaTime / AnimationTime;
                _time = _time > 1f ? 1f : _time;
                var amount = AnimationCurve.Evaluate(_time);
                var point = QuickAnimationPoint.Lerp(Start, End, amount);
                Apply(point);
            }
            else
            {
                IsPlaying = false;
                AnimationComplete.Invoke();
            }
        }
    }
}

[System.Serializable]
public class QuickAnimationPoint
{
    public QuickAnimationPoint()
    {

    }

    public QuickAnimationPoint(RectTransform fromTransform)
    {
        PositionX = fromTransform.anchoredPosition.x;
        PositionY = fromTransform.anchoredPosition.y;
        Rotation = fromTransform.rotation.eulerAngles.z;
    }

    public static QuickAnimationPoint Lerp(QuickAnimationPoint pointA, QuickAnimationPoint pointB, float amount)
    {
        return new QuickAnimationPoint
        {
            PositionX = Mathf.Lerp(pointA.PositionX, pointB.PositionX, amount),
            PositionY = Mathf.Lerp(pointA.PositionY, pointB.PositionY, amount),
            Rotation = Mathf.Lerp(pointA.Rotation, pointB.Rotation, amount)
        };
    }

    public float PositionX;
    public float PositionY;
    public float Rotation;

    public override string ToString()
    {
        return "X: " + PositionX + ", Y: " + PositionY + ", R: " + Rotation;
    }
}