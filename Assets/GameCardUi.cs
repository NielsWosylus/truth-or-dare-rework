﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameCardUi : MonoBehaviour
{
    public QuickAnimation AnimationIn;
    public QuickAnimation AnimationOut;
    public UiLabel LabelTop;
    public UiLabel LabelBottom;
    public Image IconTop;
    public Image IconBottom;
    public Text Message;

    public void Display(string message)
    {
        if(!string.IsNullOrEmpty(message))
            Message.text = message;
    }
}
