﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenLanguageButton : MonoBehaviour
{
    public Image Image;

    void Start()
    {
        LanguageManager.LanguageChanged += UpdateIcon;
        UpdateIcon(LanguageManager.CurrentLanguage);
    }

    void UpdateIcon(Language language)
    {
        Image.sprite = language.Flag;
    }
}
