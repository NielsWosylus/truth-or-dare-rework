﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RateText : MonoBehaviour
{
    public UiLabel Label;
    
    void Awake()
    {
        if(Label == null)
            Label = GetComponent<UiLabel>();
    }

    void Start()
    {
        Label.UpdatedText += UpdateLabel;
        UpdateLabel();
    }

    void UpdateLabel()
    {
        string original = Label.Text.text;
        string platformName = "";

#if UNITY_ANDROID
        platformName = "Google Play";
#endif

#if UNITY_IOS
        platformName = "App Store";
#endif

        Label.Text.text = original.Replace("[platform]", platformName);
    }
}
