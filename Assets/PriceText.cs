﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PriceText : MonoBehaviour
{
    public InAppPurchase IAP;
    public bool AllCaps;
    public string PrefixKey;
    private Text _text;

    void Awake()
    {
        _text = GetComponent<Text>();
    }

    void Start()
    {
        IAP = PriceTierSplitTester.GetLifetimeIAP();
        IapManager.Instance.InitializationComplete += (b) => UpdateLabel();
        UpdateLabel();
    }

    void UpdateLabel()
    {
        if (_text == null) return;
        _text.text = IapManager.Instance.GetPriceString(IAP.ProductId);
        if (!string.IsNullOrEmpty(PrefixKey))
            _text.text = LanguageManager.GetLabel(PrefixKey) + " " + _text.text;
        _text.text = AllCaps ? _text.text.ToUpper() : _text.text;
    }
}
