﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardChoiceController : MonoBehaviour
{
    public float SecondsUntilSpecialCard = 90;
    public float SecondsBetweenSpecialCard = 60;
    public float SpecialCardChance = 0.5f;
    public bool NextSpecialCardIsRate = true;
    public int CurrentRound;

    public int RoundsThatAlwaysHaveTruthAndDare = 5;

    public float JokerChance;

    public QuickAnimation OutroAnimation;
    public UiArea SpinArea;

    public GameCardUi DareCardUi;
    public GameCardUi TruthCardUi;
    public GameCardUi JokerCardUi;
    public GameCardUi RateCardUi;
    public GameCardUi OtoCardUi;

    public Sprite DareIcon;
    public Sprite TruthIcon;
    public Sprite JokerIcon;
    public Sprite RateIcon;
    public Sprite OtoIcon;
    public Deck DirtyDeck;

    public List<CardChoiceUi> CardChoiceUis;
    private List<Choice> _choices;
    private GameCardUi _lastDisplayedCardUi;
    private CardChoiceUi _lastSelectedCardChoiceUi;
    private bool _specialCardWasSpawnedThisRound;
    private bool _aCardWasSelectedThisRound;

    public void SelectCard(int index)
    {
        if (_aCardWasSelectedThisRound)
            return;

        var ui = CardChoiceUis[index];
        var choice = _choices[index];
        var card = choice.Ui;

        switch(choice.Type)
        {
            case ChoiceType.Truth:
                card.Display(GameController.Instance.CurrentDeck.GetNextTruth().Message 
                    + "<size=48>\n(" + LanguageManager.GetLabel("click_to_continue") + ")</size>");
                break;

            case ChoiceType.Dare:
                card.Display(GameController.Instance.CurrentDeck.GetNextDare().Message
                    + "<size=48>\n(" + LanguageManager.GetLabel("click_to_continue") + ")</size>");
                break;

            case ChoiceType.Joker:
                card.Display(GameController.Instance.CurrentDeck.GetNextJoker().Message
                    + "<size=48>\n(" + LanguageManager.GetLabel("click_to_continue") + ")</size>");
                break;
        }

        card.AnimationIn.GetStartFrom = ui.AnimationOut.GetEndFrom;
        card.AnimationIn.AnimationDelay = ui.AnimationOut.AnimationTime;
        card.AnimationIn.PlayAnimation();
        ui.AnimationOut.PlayAnimation();

        _lastDisplayedCardUi = card;
        _lastSelectedCardChoiceUi = ui;
        _aCardWasSelectedThisRound = true;
    }

    public void ClickedNext()
    {
        FadeOverlay.FadeAndDo(SpinArea.Open);
        OutroAnimation.PlayAnimation();
        _lastSelectedCardChoiceUi?.AnimationOut.ResetAnimation();
    }

    void Update()
    {
        SecondsUntilSpecialCard -= Time.deltaTime;
    }

    void OnEnable()
    {
        if(RatingManager.DoNotAsk) NextSpecialCardIsRate = false;
        if(DirtyDeck.IsUnlocked) NextSpecialCardIsRate = true;
        if(DirtyDeck.IsUnlocked && RatingManager.DoNotAsk) SecondsUntilSpecialCard = 999999;

        _lastDisplayedCardUi?.AnimationIn.ResetAnimation();
        OutroAnimation.ResetAnimation();
        GenerateChoices();
        for (int i = 0; i < 4; i++)
        {
            var ui = CardChoiceUis[i];
            ui.Animation.PlayAnimation();
            ui.Display(_choices[i].Sprite, _choices[i].Color, _choices[i].LabelText, _choices[i].Wiggle);
        }

        CurrentRound++;
        _aCardWasSelectedThisRound = false;
    }

    void GenerateChoices()
    {
        bool createRateCard = SecondsUntilSpecialCard < 0 && NextSpecialCardIsRate;
        bool createOtoCard = SecondsUntilSpecialCard < 0 && !NextSpecialCardIsRate;

        int specialCardIndex = Random.Range(0, 4);
        _specialCardWasSpawnedThisRound = false;

        _choices = new List<Choice>();
        for (int i = 0; i < 4; i++)
        {
            if(createRateCard && specialCardIndex == i)
            {
                _choices.Add(GenerateRateChoice());
                if(!DirtyDeck.IsUnlocked) NextSpecialCardIsRate = false;
                SecondsUntilSpecialCard = SecondsBetweenSpecialCard;
                _specialCardWasSpawnedThisRound = true;
                continue;
            }

            if(createOtoCard && specialCardIndex == i)
            {
                _choices.Add(RMConfig.GetBool("EnableSale_bool") ? GenerateOtoChoice() : GenerateChoice());
                if(!RatingManager.HasRated) NextSpecialCardIsRate = true;
                SecondsUntilSpecialCard = SecondsBetweenSpecialCard;
                _specialCardWasSpawnedThisRound = true;
                continue;
            }

            _choices.Add(GenerateChoice());
        }

        if(CurrentRound <= RoundsThatAlwaysHaveTruthAndDare)
            EnsureTruthAndDare();
    }

    void EnsureTruthAndDare()
    {
        bool containsTruth = false;
        bool containsDare = false;
        foreach (var choice in _choices)
        {
            if (choice.Type == ChoiceType.Truth)
            {
                containsTruth = true;
            }
            else if (choice.Type == ChoiceType.Dare)
            {
                containsDare = true;
            }
        }

        if (!containsTruth)
        {
            int index = Random.Range(0, 4);
            _choices[index] = new Choice
            {
                Ui = TruthCardUi,
                Sprite = TruthIcon,
                Type = ChoiceType.Truth,
                Color = PlayerManager.CurrentPlayer.Color
            };
        }
        else if (!containsDare)
        {
            int index = Random.Range(0, 4);
            _choices[index] = new Choice
            {
                Ui = DareCardUi,
                Sprite = DareIcon,
                Type = ChoiceType.Dare,
                Color = PlayerManager.CurrentPlayer.Color
            };
        }
    }

    Choice GenerateRateChoice()
    {
        return new Choice
        {
            Ui = RateCardUi,
            Sprite = RateIcon,
            Color = Color.white,
            Type = ChoiceType.Rate,
            Wiggle = true
        };
    }

    Choice GenerateOtoChoice()
    {
        return new Choice
        {
            Ui = OtoCardUi,
            Sprite = OtoIcon,
            Color = Color.white,
            Type = ChoiceType.Oto,
            Wiggle = true
        };
    }

    Choice GenerateChoice()
    {
        var choice = new Choice();
        float a = Random.Range(0f, 1f);

        //JOKER
        if (a < JokerChance && !_specialCardWasSpawnedThisRound && SecondsUntilSpecialCard > 0 && CurrentRound > RoundsThatAlwaysHaveTruthAndDare)
        {
            choice.Ui = JokerCardUi;
            choice.Sprite = JokerIcon;
            choice.Color = Color.white;
            choice.Type = ChoiceType.Joker;
            choice.LabelText = LanguageManager.GetLabel("joker").ToUpper();
            return choice;
        }

        if (Random.Range(0f, 1f) < 0.5f)
        {
            choice.Ui = TruthCardUi;
            choice.Sprite = TruthIcon;
            choice.Type = ChoiceType.Truth;
            choice.LabelText = LanguageManager.GetLabel("truth").ToUpper();
        }
        else
        {
            choice.Ui = DareCardUi;
            choice.Sprite = DareIcon;
            choice.Type = ChoiceType.Dare;
            choice.LabelText = LanguageManager.GetLabel("dare").ToUpper();
        }

        choice.Color = PlayerManager.CurrentPlayer.Color;
        return choice;
    }

    private class Choice
    {
        public GameCardUi Ui;
        public Color Color;
        public Sprite Sprite;
        public ChoiceType Type;
        public bool Wiggle;
        public string LabelText;
    }

    private enum ChoiceType
    {
        Truth,
        Dare,
        Joker,
        Oto,
        Rate
    }
}
