﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.Events;

public class RouletteWheel : MonoBehaviour
{
    public UiArea NextArea;
    public Transform PlayerBulbPrefab;
    public Transform RotationPoint;
    public bool Randomize;
    public float LerpSpeed;
    public float MaxSpeed;
    public float PostSpinDelay;
    public UnityEvent SpinComplete;

    private float _rotation;
    public float Rotation
    {
        get => _rotation;
        set
        {
            RotationPoint.rotation = Quaternion.Euler(0, 0, value);
            _rotation = value;
        }
    }

    private List<PlayerBulb> _playerBulbs;
    private int _currentPlayerIndex;
    private int _selectedPlayerIndex;
    private float DegreesPerPlayer => 360f / PlayerManager.Instance.PlayerCount;
    private float _desiredRotation;
    private float _rotationSpeed;
    private float _postSpinDelay;
    private bool _isSpinning;
    private List<int> _playerOrder;
    private int _playerOrderIndex;


    public void Spin()
    {
        if (_isSpinning)
            return;

        int add = 1;
        if (Randomize)
        {
            int id = GetNextPlayerIndex();
            if (id == _selectedPlayerIndex)
                add = 0;
            else if (id > _selectedPlayerIndex)
                add = id - _selectedPlayerIndex;
            else
            {
                add = PlayerManager.Instance.PlayerCount + id - _selectedPlayerIndex;
            }
        }

        _selectedPlayerIndex = _currentPlayerIndex + add;
        if (_selectedPlayerIndex >= PlayerManager.Instance.PlayerCount)
            _selectedPlayerIndex -= PlayerManager.Instance.PlayerCount;

        _desiredRotation = Rotation + add * DegreesPerPlayer;
        if(Randomize) _desiredRotation += 360f * Random.Range(1, 4);
        _isSpinning = true;
        _postSpinDelay = PostSpinDelay;
    }

    void Update()
    {
        if (!_isSpinning)
            return;

        float diff = Mathf.Lerp(Rotation, _desiredRotation, LerpSpeed * Time.deltaTime) - Rotation;
        diff = diff > MaxSpeed ? MaxSpeed : diff;

        _rotationSpeed = Mathf.Lerp(_rotationSpeed, diff, LerpSpeed * Time.deltaTime);
        Rotation += _rotationSpeed;

        float threshold = 0.1f;
        if (Mathf.Abs(Rotation - _desiredRotation) < threshold && Mathf.Abs(_rotationSpeed) < threshold)
        {
            _postSpinDelay -= Time.deltaTime;
            if(_postSpinDelay <= 0)
                CompleteSpinning();
        }
    }

    void OnEnable()
    {
        DestroyBulbs();
        Populate();
        ResetRotation();
    }

    void CompleteSpinning()
    {
        Debug.Log("Completed spinning.");
        Rotation = _desiredRotation;
        _rotationSpeed = 0;
        _isSpinning = false;
        _postSpinDelay = PostSpinDelay;
        PlayerManager.CurrentPlayer = _playerBulbs[_selectedPlayerIndex].Player;
        SpinComplete.Invoke();
        FadeOverlay.FadeAndDo(NextArea.Open);
    }

    void Populate()
    {
        _playerBulbs = new List<PlayerBulb>();

        for (int i = 0; i < PlayerManager.Instance.PlayerCount; i++)
        {
            var bulb = Instantiate(PlayerBulbPrefab, RotationPoint).GetComponent<PlayerBulb>();
            bulb.transform.localRotation = Quaternion.Euler(0, 0, 360f - i * DegreesPerPlayer);
            bulb.Display(PlayerManager.Instance.Players[i]);
            _playerBulbs.Add(bulb);
        }
    }

    void DestroyBulbs()
    {
        if (_playerBulbs != null)
        {
            foreach (var bulb in _playerBulbs)
            {
                Destroy(bulb.gameObject);
            }
        }
    }

    void ResetRotation()
    {
        if (PlayerManager.CurrentPlayer != null)
            _currentPlayerIndex = PlayerManager.Instance.Players.IndexOf(PlayerManager.CurrentPlayer);
        else _currentPlayerIndex = 0;

        Rotation = _currentPlayerIndex * DegreesPerPlayer;
        _desiredRotation = Rotation;
        _rotationSpeed = 0;
    }

    int GetNextPlayerIndex()
    {
        if(_playerOrder == null)
            ResetPlayerOrder();
        else if (_playerOrderIndex >= _playerOrder.Count || _playerOrder.Count != PlayerManager.Instance.PlayerCount)
            ResetPlayerOrder();

        int selected = _playerOrder[_playerOrderIndex];
        _playerOrderIndex++;
        return selected;
    }

    void ResetPlayerOrder()
    {
        _playerOrderIndex = 0;
        _playerOrder = new List<int>(16);
        for (int i = 0; i < PlayerManager.Instance.PlayerCount; i++)
            { _playerOrder.Add(i); }
        _playerOrder.Shuffle();
    }
}
