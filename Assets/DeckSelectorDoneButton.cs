﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DeckSelectorDoneButton : MonoBehaviour
{
    public Image Image;
    public Color EnabledColor;
    public Color DisabledColor;
    public UnityEvent Done;

    void Start()
    {
        GameController.Instance.DeckChanged += (deck) => Image.color = EnabledColor;
        Image.color = GameController.Instance.CurrentDeck != null ? EnabledColor : DisabledColor;
    }

    public void Clicked()
    {
        if(GameController.Instance.CurrentDeck != null)
            Done.Invoke();
    }
}