﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurchasePanelOpener : MonoBehaviour
{
    public int RoundsBetweenOpen;
    public int RoundsBetweenOpenWithOneMissing;
    public bool NextIsRate = true;
    public UiPopup OtoPopup;
    public Deck DirtyDeck;
    public UiPopup RatePopup;
    private int _rounds;

    void OnEnable()
    {
        if (DirtyDeck.IsUnlocked)
        {
            NextIsRate = true;
            RoundsBetweenOpen = RoundsBetweenOpenWithOneMissing;
            if (RatingManager.DoNotAsk)
                return;
        }

        if (RatingManager.DoNotAsk)
        {
            NextIsRate = false;
            RoundsBetweenOpen = RoundsBetweenOpenWithOneMissing;
        }

        _rounds++;
        if(_rounds >= RoundsBetweenOpen)
        {
            if (NextIsRate)
                RatePopup.Open();
            else OtoPopup.Open();
            _rounds = 0;
            NextIsRate = !NextIsRate;
        }
    }

    public void ResetRoundCount()
    {
        _rounds = 0;
    }
}
