﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableWhenDirtyIsOwned : MonoBehaviour
{
    public Deck Deck;

    void Update()
    {
        if(Deck.IsUnlocked)
            gameObject.SetActive(false);
    }
}
