﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShuffleToggle : MonoBehaviour
{
    public Image Image;
    public Sprite OnSprite;
    public Sprite OffSprite;
    public RouletteWheel Wheel;

    private bool On
    {
        get => Wheel.Randomize;
        set
        {
            Wheel.Randomize = value;
            Image.sprite = value ? OnSprite : OffSprite;
        }
    }

    void OnEnable()
    {
        Image.sprite = On ? OnSprite : OffSprite;
    }

    public void Clicked()
    {
        On = !On;
    }
    
}
