﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

public class LegacyDataManager {

	public static AppData appData = new AppData();
    public static string saveFileName = "appdata.dat";
    public static DateTime timeOfAppOpened = DateTime.Now;
    public static TimeSpan timeSpendInSession
    {
        get
        {
            return DateTime.Now - timeOfAppOpened;
        }
    }

    public static void WipeSave()
    {
        Debug.Log("Wiping saved data");
        appData = new AppData();
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/"+saveFileName);
        bf.Serialize(file, appData);
        file.Close();
    }

    public static void Load (InAppPurchase dirtyIAP)
    {
        if(dirtyIAP.IsOwned)
        {
            Debug.Log("Skipping legacy check, dirty already owned");
            return;
        }

        string path = Application.persistentDataPath + "/" + saveFileName;


        try
        {
            if (!File.Exists(path))
            {
                Debug.Log("No legacy save file to load.");
                return;
            }

            Debug.Log("Loading legacy data...");


            AppData data = null;
            using (FileStream fileStream = File.Open(path, FileMode.Open))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                data = (AppData)formatter.Deserialize(fileStream);
            }

            appData = data;
            if (appData.themeData != null)
            {
                foreach (ThemeData tdata in appData.themeData)
                {
                    Debug.Log("Loaded theme "+tdata.name
                    +", owned: "+tdata.owned
                    +", expires: "+tdata.expirationTime.ToLongTimeString());

                    if(tdata.name == "dirty")
                    {
                        if(tdata.owned)
                        {
                            dirtyIAP.OnPurchaseSuccess();
                        }
                    }
                }
            }
        }
        catch(Exception e)
        {
            Debug.LogError(e);
        }
    }
}

[Serializable]
public class AppData
{
    public List<ThemeData> themeData;
    public SystemLanguage language = SystemLanguage.English;
    public bool languageSet;
    public bool isVersionA = true;
    public int timesOpened;
    public bool hasRated;
    public bool hasRecievedFreePack;
}

[Serializable]
public class ThemeData
{
	public bool owned;
	public DateTime expirationTime;
	public string name;
    public bool wasPreviouslyRented;
}
