﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RateCard : MonoBehaviour
{
    public GameObject RateFilterParent;
    public GameObject AskRateParent;

    void OnEnable()
    {
        RateFilterParent.SetActive(true);
        AskRateParent.SetActive(false);
    }
}
