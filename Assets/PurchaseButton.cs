﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PurchaseButton : MonoBehaviour
{
    public LoadingIndicator LoadingIndicator;
    public GameObject[] Graphics;
    
    public Deck Deck;
    public InAppPurchase IAP;
    public UnityEvent PurchaseSuccessful;
    public bool _isLoading;

    void Awake()
    {
        IAP = PriceTierSplitTester.GetLifetimeIAP();
        IAP.PurchaseBegun += ()=> SetLoading(true);
        IAP.PurchaseComplete += b =>
        {
            Debug.Log("Purchase complete");
            SetLoading(false);
            if(b) PurchaseSuccessful.Invoke();
        };
    }

    // void OnEnable()
    // {
    //     SetLoading(false);
    // }

    public void Clicked()
    {
        if (_isLoading)
            return;

        Debug.Log("Purchase clicked");
        IAP.Purchase();
    }

    void SetLoading(bool isLoading)
    {
        Debug.Log("Setting loading");
        _isLoading = isLoading;
        LoadingIndicator.gameObject.SetActive(isLoading);
        foreach(var item in Graphics)
        {
            item.SetActive(!isLoading);
        }
    }
}
