﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SampleCycler : MonoBehaviour
{
    public string[] SampleKeys;

    public UiLabel Label;
    public MaskableGraphic LabelGraphic;
    public MaskableGraphic[] CardGraphics;
    public Color CardHighlitColor;
    public Color CardUnlitColor;
    public float CycleTime;
    public float CardFadeTime;
    public float _time;
    public int _cycleIndex;
    public float[] _cardLight = {0, 0, 0};
    public bool _fadingOut;
    public bool _fadingIn;
    public string _fadingToKey;
    public float _fadeAmount;

    void OnEnable()
    {
        Restart();
    }

    public void Restart()
    {
        _time = CycleTime;
        _fadeAmount = 1f;
        _fadingIn = false;
        _fadingOut = false;
        _cycleIndex = 0;
        LabelGraphic.color = MyUtil.ColorWithNewAlpha(LabelGraphic.color, _fadeAmount);
        Label.SetKey(SampleKeys[_cycleIndex]);
    }

    void Update()
    {
        _time -= Time.deltaTime;
        if(_time <= 0)
            CycleToNext();

        
        for(int i = 0; i < 3; i++)
        {
            if(i == _cycleIndex)
            {
                _cardLight[i] = Mathf.Min(_cardLight[i] + Time.deltaTime / CardFadeTime, 1f);
            }
            else
            {
                _cardLight[i] = Mathf.Max(_cardLight[i] - Time.deltaTime / CardFadeTime, 0f);
            }

            CardGraphics[i].color = Color.Lerp(CardUnlitColor, CardHighlitColor, _cardLight[i]);
        }

        if(_fadingIn)
        {
            _fadeAmount = Mathf.Min(_fadeAmount + Time.deltaTime / CardFadeTime, 1f);
            LabelGraphic.color = MyUtil.ColorWithNewAlpha(LabelGraphic.color, _fadeAmount);
            if(_fadeAmount == 1f)
                _fadingIn = false;
        }
        else if (_fadingOut)
        {
            _fadeAmount -= Time.deltaTime / CardFadeTime;
            if(_fadeAmount <= 0)
            {
                _fadeAmount = 0;
                _fadingOut = false;
                _fadingIn = true;
                Label.SetKey(_fadingToKey);
            }
            LabelGraphic.color = MyUtil.ColorWithNewAlpha(LabelGraphic.color, _fadeAmount);
        }
    }

    void CycleToNext()
    {
        SetCycleIndex(_cycleIndex >= 2 ? 0 : _cycleIndex + 1);
    }

    public void SetCycleIndex(int index)
    {
        _time = CycleTime;
        if(index == _cycleIndex)
            return;

        _cycleIndex = index;
        _fadingToKey = SampleKeys[_cycleIndex];
        _fadingIn = false;
        _fadingOut = true;

    }
}
