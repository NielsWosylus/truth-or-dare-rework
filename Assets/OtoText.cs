﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtoText : MonoBehaviour
{
    public UiLabel Label;
    public InAppPurchase IAP;
    public float Multiplier;
    public bool RoundPrice;
    
    void Awake()
    {
        if(Label == null)
            Label = GetComponent<UiLabel>();
    }

    void Start()
    {
        Label.UpdatedText += UpdateLabel;
        IapManager.Instance.InitializationComplete += b => UpdateLabel();
        UpdateLabel();
    }

    void UpdateLabel()
    {
        if(Label.Text == null)
            return;

        string original = Label.Text.text;
        string pass1 = original.Replace("[price0]", IapManager.Instance.GetPriceString(IAP.ProductId));

        float realAmount = IapManager.Instance.GetPrice(IAP.ProductId);
        float multipliedAmount = realAmount * Multiplier;
        if(RoundPrice) multipliedAmount = Mathf.Round(multipliedAmount) - 0.01f;

        float saveAmount = multipliedAmount - realAmount;
        string saveString = saveAmount.ToString("0.00") + IapManager.Instance.GetCurrencyCode(IAP.ProductId);

        Label.Text.text = pass1.Replace("[price1]", saveString);
    }
}
