﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeOverlay : MonoBehaviour
{
    private static FadeOverlay _instance;

    public static void FadeAndDo(Action action)
    {
        _instance._fadeAction = action;
        _instance._fadingIn = true;
        _instance._myImage.enabled = true;
    }

    public float FadeTime;
    private Image _myImage;
    private bool _fadingIn;
    private float _alpha;
    private Action _fadeAction;

    public void FadeToArea(UiArea area)
    {
        FadeAndDo(area.Open);
    }

    void Awake()
    {
        _instance = this;
        _myImage = GetComponent<Image>();
    }

    void Update()
    {
        if (_fadingIn)
        {
            if (_alpha < 1f)
            {
                _alpha += Time.deltaTime / FadeTime;
                _myImage.color = new Color(0, 0, 0, _alpha);
            }
            else
            {
                _fadingIn = false;
                _fadeAction?.Invoke();
            }
        }
        else
        {
            if (_alpha > 0)
            {
                _alpha -= Time.deltaTime / FadeTime;
                _myImage.color = new Color(0, 0, 0, _alpha);
            }
            else
            {
                if (_myImage.enabled)
                    _myImage.enabled = false;
            }
        }
    }
}
