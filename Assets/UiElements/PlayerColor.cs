﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerColor : MonoBehaviour
{
    private MaskableGraphic _myGraphic;

    void Awake()
    {
        _myGraphic = GetComponent<MaskableGraphic>();
    }

    void Start()
    {
        PlayerManager.CurrentPlayerChanged += CurrentPlayerChanged;
        CurrentPlayerChanged(PlayerManager.CurrentPlayer);
    }

    private void CurrentPlayerChanged(Player player)
    {
        _myGraphic.color = player.Color;
    }
}
