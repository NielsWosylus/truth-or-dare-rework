﻿using System.Collections.Generic;
using UnityEngine;

namespace UiElements
{
    public class PlayerList : MonoBehaviour
    {
        public PlayerManager PlayerManager;
        public List<PlayerEntry> Entries;

        void Start()
        {
            Display();
        }

        public void Save()
        {
            for (int i = 0; i < PlayerManager.PlayerCount; i++)
            {
                PlayerManager.Players[i].Name = Entries[i].InputField.text;
            }

            PlayerManager.SavePlayers();
        }

        public void AddPlayer()
        {
            if (PlayerManager.PlayerCount >= PlayerManager.MaxPlayerCount)
                return;

            Entries[PlayerManager.PlayerCount].gameObject.SetActive(true);
            PlayerManager.PlayerCount++;
        }

        public void RemovePlayer()
        {
            if (PlayerManager.PlayerCount <= 2)
                return;

            PlayerManager.PlayerCount--;
            Entries[PlayerManager.PlayerCount].gameObject.SetActive(false);
        }

        void Display()
        {
            for (int i = 0; i < Entries.Count; i++)
            {
                var entry = Entries[i];
                entry.Display(PlayerManager.Players[i]);
                Entries[i].gameObject.SetActive(i < PlayerManager.PlayerCount);
            }
        }
    }
}
