﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardChoiceUi : MonoBehaviour
{
    public QuickAnimation Animation;
    public QuickAnimation AnimationOut;
    public Image Icon;
    public Text LabelText;
    public MaskableGraphic[] ColoredGraphics;

    public bool Wiggle;
    public float SecondsBetweenWiggles = 3f;
    public float WiggleAnimationTime;
    public AnimationCurve WiggleCurve;
    public float WiggleMaxRotation;
    private float _secondsUntilWiggle;
    private float _time;
    private bool _isWiggling;

    void Awake()
    {
        Animation.Started += StopWiggle;
        AnimationOut.Started += StopWiggle;
    }

    void Update()
    {
        _secondsUntilWiggle -= Time.deltaTime;
        if(Wiggle && _secondsUntilWiggle <= 0)
        {
            PlayWiggle();
        }

        if(_isWiggling)
        {
            _time += Time.deltaTime / WiggleAnimationTime;

            if(_time > 1)
                _time = 1;

            float rotation = WiggleCurve.Evaluate(_time) * WiggleMaxRotation;
            transform.rotation = Quaternion.Euler(0, 0, rotation);

            if(_time == 1)
                StopWiggle();
        }
    }

    void StopWiggle()
    {
        _isWiggling = false;
        _time = 0;
        transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    void PlayWiggle()
    {
        _secondsUntilWiggle = SecondsBetweenWiggles;
        _isWiggling = true;
    }


    public void Display(Sprite icon, Color color, string labelText, bool wiggle)
    {
        Icon.sprite = icon;
        Wiggle = wiggle;
        if(LabelText != null)
            LabelText.text = labelText;
        _secondsUntilWiggle = SecondsBetweenWiggles;
        foreach (var graphic in ColoredGraphics)
        {
            graphic.color = color;
        }
    }
}

