﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageButton : MonoBehaviour
{
    public Language Language;
    public Image Image;

    void Start()
    {
        LanguageManager.LanguageChanged += OnLanguageChanged;
    }

    private void OnLanguageChanged(Language language)
    {
        Image.color = MyUtil.ColorWithNewAlpha(Image.color, language == Language ? 1f : 0.5f);
    }

    public void Display(Language language)
    {
        Language = language;
        Image.sprite = language.Flag;
        OnLanguageChanged(LanguageManager.CurrentLanguage);
    }

    public void Pressed()
    {
        LanguageManager.CurrentLanguage = Language;
    }
}
