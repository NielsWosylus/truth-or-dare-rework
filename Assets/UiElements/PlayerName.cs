﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerName : MonoBehaviour
{
    public bool AllCaps;
    private Text _text;

    void Start()
    {
        _text = GetComponent<Text>();
        PlayerManager.CurrentPlayerChanged += CurrentPlayerChanged;
        CurrentPlayerChanged(PlayerManager.CurrentPlayer);
    }

    private void CurrentPlayerChanged(Player player)
    {
        if (player == null)
        {
            _text.text = "";
            return;
        }

        _text.text = AllCaps ? player.Name.ToUpper() : player.Name;
    }
}
