﻿using UnityEngine;
using UnityEngine.UI;

namespace UiElements
{
    public class PlayerEntry : MonoBehaviour
    {
        public Player Player;
        public Image PlayerIcon;
        public InputField InputField;

        public void Display(Player player)
        {
            Player = player;
            PlayerIcon.color = player.Color;
            InputField.text = player.Name;
        }
    }
}
