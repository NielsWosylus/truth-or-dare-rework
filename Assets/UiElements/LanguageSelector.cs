﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageSelector : MonoBehaviour
{
    public LanguageManager LanguageManager;
    public Transform ButtonPrefab;

    void Start()
    {
        foreach (var language in LanguageManager.AvailableLanguages)
        {
            Instantiate(ButtonPrefab, transform).GetComponent<LanguageButton>().Display(language);
        }
    }
}
