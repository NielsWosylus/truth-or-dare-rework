﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBulb : MonoBehaviour
{
    public Image Background;
    public Player Player;

    public void Display(Player player)
    {
        Player = player;
        Background.color = player.Color;
    }
}
