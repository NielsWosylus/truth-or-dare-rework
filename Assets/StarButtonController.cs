﻿using System.Collections;
using System.Collections.Generic;
using Firebase.Analytics;
using UnityEngine;
using UnityEngine.UI;

public class StarButtonController : MonoBehaviour
{
    public int StarsBeforeAskRealRate = 5;
    public int StarsBeforeNeverAskAgain = 3;
    public Sprite FullStar;
    public Sprite EmptyStar;
    public List<Image> StarImages;
    public MaskableGraphic DoneGraphic;
    public CardChoiceController Controller;
    public UiPopup RatingPopup;
    public RateCard RateCard;

    private int _starAmount;

    public int StarAmount
    {
        get => _starAmount;
        set
        {
            _starAmount = value;
            for (int i = 0; i < 5; i++)
            {
                StarImages[i].sprite = i < value ? FullStar : EmptyStar;
            }

            DoneGraphic.color = MyUtil.ColorWithNewAlpha(DoneGraphic.color, value == 0 ? 0.5f : 1f);

        }
    }

    void OnEnable()
    {
        StarAmount = 0;
    }

    public void ClickedStar(int index)
    {
        StarAmount = index;
    }

    public void ClickedDone()
    {
        if (StarAmount == 0)
            return;

        if(StarAmount <= StarsBeforeNeverAskAgain)
            RatingManager.NeverAskAgain();

        if (StarAmount < StarsBeforeAskRealRate)
        {
            if (Controller != null)
            {
                Controller.ClickedNext();
                return;
            }

            RatingPopup.Close();
            FirebaseAnalytics.LogEvent("Ratings_Dislike");
        }

        RateCard.RateFilterParent.SetActive(false);
        RateCard.AskRateParent.SetActive(true);
        FirebaseAnalytics.LogEvent("Ratings_Like");
    }
}